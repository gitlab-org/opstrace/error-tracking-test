#!/usr/bin/env python
import sentry_sdk, os, sys
from sentry_sdk import Hub
from sentry_sdk.sessions import auto_session_tracking
import time

def init_sentry(dsn: str) -> None:

    sentry_sdk.init(
        dsn=dsn,
        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production.
        traces_sample_rate=1.0,
	    debug=True,
        release="v1.2.3",
        environment="dev",
        auto_session_tracking=True
    )


def failing_function() -> None:
    raise Exception("An exception")

if __name__ == "__main__":
    dsn = os.getenv("SENTRY_DSN")
    if dsn is None:
        print("empty dsn; exiting")
        sys.exit(1)

    init_sentry(dsn)

    hub = Hub.current
    hub.start_session()

    try:
        with hub.configure_scope() as scope:
            scope.set_user({"id": "42"})
            raise Exception("all is wrong")
    except Exception:
        hub.capture_exception()
    # time.sleep(100)
    hub.end_session()
    hub.flush()


