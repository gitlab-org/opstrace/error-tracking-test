# error-tracking-test Javascript

In order to run this application make sure that `SENTRY_DSN` env var exists

```
export SENTRY_DSN=<SENTRY_DSN>
cd app
cargo build
cargo run
```


Example of crashed session.js
```
{}
{"type":"session","length":209}
{
    "sid":"1cf6f360-dc40-4a74-abf8-2c5f02c77ee3",
    "did":null,
    "started":"2023-01-11T14:11:12.91003Z",
    "init":true,
    "duration":2.958e-6,
    "status":"crashed",
    "errors":0,
    "attrs":{
        "release":"app@1.0.0","environment":"dev"
    }
}

```

Example of an exited session.js
```
{"type":"session","length":209}
{
    "sid":"d24a6776-0076-4160-955d-334f303d5900",
    "did":null,
    "started":"2023-01-11T14:14:16.674585Z",
    "init":true,
    "duration":2.958e-6,
    "status":"exited",
    "errors":0,
    "attrs":{
        "release":"app@1.0.0","environment":"dev"
    }
}

```