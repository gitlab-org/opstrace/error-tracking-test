use sentry;
use std::env;

fn main() {

    let lang = env::var("SENTRY_DSN").expect("$SENTRY_DSN is not set");

    let _guard = sentry::init((lang, sentry::ClientOptions {
        release: sentry::release_name!(),
        debug: true, // <- this should only be used during development
        environment: Some("dev".into()),
        ..Default::default()
    }));

    sentry::start_session();

    sentry::end_session_with_status( sentry::types::protocol::v7::SessionStatus::Crashed);
}

