# error-tracking-test

This repo contains scripts that are used for blackbox testing of Gitlab Error Tracking feature.
Relies on `SENTRY_DSN` environment variable that is available via CI.

### Scheduled tests

We've a scheduled pipeline that runs every 5 minutes and sends errors via the DSN enabled for this project.

Errors sent can be seen on Monitor > Errors Tracking tab in Gitlab UI.

