
const Sentry = require('@sentry/node');


Sentry.init({
  dsn: process.env.SENTRY_DSN,
  debug: true,
  release:"my-javascriprt-project@1.0.0",
  environment: "dev",
  attachStacktrace: true // This option will attach the stacktrace in the Message payload
});

Sentry.captureMessage('Testing captureMessage from JS Sentry SDK');

