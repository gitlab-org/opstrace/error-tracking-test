# error-tracking-test Javascript - Message

In order to run this application make sure that `SENTRY_DSN` env var exists

```
export SENTRY_DSN=<SENTRY_DSN>
npm install
node app.js
node session_exited.js
```

The payload for captureMessage is being sent on the StoreEndpoint and looks like the following:
```
{
    "event_id": "f27c137362f04b57b89e92ed95bd4cff",
    "level": "info",
    "message": "Testing captureMessage from JS Sentry SDK",
    "platform": "node",
    "timestamp": 1673516764.125,
    "environment": "dev",
    "release": "my-javascriprt-project@1.0.0",
    "sdk": {
        "integrations": [
            "InboundFilters",
            "FunctionToString",
            "ContextLines",
            "Console",
            "Http",
            "OnUncaughtException",
            "OnUnhandledRejection",
            "LinkedErrors"
        ],
        "name": "sentry.javascript.node",
        "version": "6.19.7",
        "packages": [
            {
                "name": "npm:@sentry/node",
                "version": "6.19.7"
            }
        ]
    },
    "tags": {},
    "extra": {}
}
```

With `attachStacktrace` option to true during Sentry SDK init the payload has stacktrace:
```
{
    "event_id": "262eba7feac14e5facb28dd72f918e5d",
    "level": "info",
    "message": "Testing captureMessage from JS Sentry SDK",
    "stacktrace": {
        "frames": [
            {
                "filename": "node:internal/main/run_main_module",
                "module": "run_main_module",
                "function": "<anonymous>",
                "lineno": 17,
                "colno": 47,
                "in_app": false
            },
            {
                "filename": "node:internal/modules/run_main",
                "module": "run_main",
                "function": "Function.executeUserEntryPoint [as runMain]",
                "lineno": 77,
                "colno": 12,
                "in_app": false
            },
            {
                "filename": "node:internal/modules/cjs/loader",
                "module": "loader",
                "function": "Module._load",
                "lineno": 822,
                "colno": 12,
                "in_app": false
            },
            {
                "filename": "node:internal/modules/cjs/loader",
                "module": "loader",
                "function": "Module.load",
                "lineno": 981,
                "colno": 32,
                "in_app": false
            },
            {
                "filename": "node:internal/modules/cjs/loader",
                "module": "loader",
                "function": "Module._extensions..js",
                "lineno": 1159,
                "colno": 10,
                "in_app": false
            },
            {
                "filename": "node:internal/modules/cjs/loader",
                "module": "loader",
                "function": "Module._compile",
                "lineno": 1105,
                "colno": 14,
                "in_app": false
            },
            {
                "filename": "/Users/nickilieskou/projects/error-tracking-test/messages/javascript/app.js",
                "module": "app",
                "function": "Object.<anonymous>",
                "lineno": 13,
                "colno": 8,
                "in_app": true
            }
        ]
    },
    "platform": "node",
    "timestamp": 1673517760.84,
    "environment": "dev",
    "release": "my-javascriprt-project@1.0.0",
    "sdk": {
        "integrations": [
            "InboundFilters",
            "FunctionToString",
            "ContextLines",
            "Console",
            "Http",
            "OnUncaughtException",
            "OnUnhandledRejection",
            "LinkedErrors"
        ],
        "name": "sentry.javascript.node",
        "version": "6.19.7",
        "packages": [
            {
                "name": "npm:@sentry/node",
                "version": "6.19.7"
            }
        ]
    },
    "tags": {},
    "extra": {}
}
```