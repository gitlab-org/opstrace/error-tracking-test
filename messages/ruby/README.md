# error-tracking-test Ruby

In order to run this application make sure that `SENTRY_DSN` env var exists

```
export SENTRY_DSN=<SENTRY_DSN>
bundle install
ruby app.rb
```

The message payload is being sent in the envelope endpoint and it contains by default a stacktrace:
```
{
    "event_id": "d6839bd298154388ab1bb688adc92097",
    "dsn": "http://user@localhost:8080/projects/32149347",
    "sdk": {
        "name": "sentry.ruby",
        "version": "5.7.0"
    },
    "sent_at": "2023-01-12T09:50:47Z"
}
{
    "type": "event",
    "content_type": "application/json"
}
{
    "event_id": "d6839bd298154388ab1bb688adc92097",
    "level": "error",
    "timestamp": "2023-01-12T09:50:47Z",
    "release": "v1.0.0",
    "environment": "dev",
    "server_name": "Nicks-MacBook-Pro.local",
    "modules": {
        "did_you_mean": "1.5.0",
        "concurrent-ruby": "1.1.10",
        "sentry-ruby": "5.7.0",
        "english": "0.7.1",
        "forwardable": "1.3.2",
        "date": "3.1.3",
        "time": "0.1.0",
        "bigdecimal": "3.0.0",
        "etc": "1.3.0",
        "timeout": "0.1.1",
        "logger": "1.4.3",
        "uri": "0.10.1",
        "io-wait": "0.2.0",
        "securerandom": "0.1.0",
        "set": "1.0.1",
        "ipaddr": "1.2.2",
        "cgi": "0.2.1",
        "json": "2.5.1",
        "ostruct": "0.3.1",
        "base64": "0.1.0",
        "net-protocol": "0.1.1",
        "net-http": "0.1.1",
        "zlib": "2.0.0",
        "stringio": "3.0.1",
        "rake": "13.0.3",
        "fileutils": "1.5.0",
        "singleton": "0.1.1",
        "optparse": "0.1.1",
        "abbrev": "0.1.0",
        "benchmark": "0.1.1",
        "bundler": "2.2.33",
        "csv": "3.1.9",
        "dbm": "1.1.0",
        "debug": "0.2.1",
        "delegate": "0.2.0",
        "digest": "3.0.0",
        "drb": "2.0.5",
        "erb": "2.2.0",
        "fcntl": "1.0.1",
        "fiddle": "1.0.8",
        "find": "0.1.0",
        "getoptlong": "0.1.1",
        "io-console": "0.5.7",
        "io-nonblock": "0.1.0",
        "irb": "1.3.5",
        "matrix": "0.3.1",
        "minitest": "5.14.2",
        "mutex_m": "0.1.1",
        "net-ftp": "0.1.2",
        "net-imap": "0.1.1",
        "net-pop": "0.1.1",
        "net-smtp": "0.2.1",
        "nkf": "0.1.0",
        "observer": "0.1.1",
        "open-uri": "0.1.0",
        "open3": "0.1.1",
        "openssl": "2.2.1",
        "pathname": "0.1.0",
        "power_assert": "1.2.0",
        "pp": "0.2.1",
        "prettyprint": "0.1.1",
        "prime": "0.1.2",
        "pstore": "0.1.1",
        "psych": "3.3.2",
        "racc": "1.5.2",
        "rbs": "1.4.0",
        "rdoc": "6.3.3",
        "readline": "0.0.2",
        "readline-ext": "0.1.1",
        "reline": "0.2.5",
        "resolv": "0.2.1",
        "resolv-replace": "0.1.0",
        "rexml": "3.2.5",
        "rinda": "0.1.1",
        "rss": "0.2.9",
        "shellwords": "0.1.0",
        "strscan": "3.0.1",
        "syslog": "0.1.0",
        "tempfile": "0.1.1",
        "test-unit": "3.3.7",
        "tmpdir": "0.1.2",
        "tracer": "0.1.1",
        "tsort": "0.1.0",
        "typeprof": "0.15.2",
        "un": "0.1.0",
        "weakref": "0.1.1",
        "yaml": "0.1.1"
    },
    "message": "Testing capture_message fropm Ruby Sentry SDK",
    "user": {},
    "tags": {},
    "contexts": {
        "os": {
            "name": "Darwin",
            "version": "Darwin Kernel Version 22.2.0: Fri Nov 11 02:03:51 PST 2022; root:xnu-8792.61.2~4/RELEASE_ARM64_T6000",
            "build": "22.2.0",
            "kernel_version": "Darwin Kernel Version 22.2.0: Fri Nov 11 02:03:51 PST 2022; root:xnu-8792.61.2~4/RELEASE_ARM64_T6000"
        },
        "runtime": {
            "name": "ruby",
            "version": "ruby 3.0.4p208 (2022-04-12 revision 3fa771dded) [arm64-darwin21]"
        }
    },
    "extra": {},
    "fingerprint": [],
    "breadcrumbs": {
        "values": []
    },
    "platform": "ruby",
    "sdk": {
        "name": "sentry.ruby",
        "version": "5.7.0"
    },
    "type": "event",
    "threads": {
        "values": [
            {
                "id": 260,
                "name": null,
                "crashed": false,
                "current": true,
                "stacktrace": {
                    "frames": [
                        {
                            "project_root": "/Users/nickilieskou/projects/error-tracking-test/messages/ruby",
                            "abs_path": "app.rb",
                            "function": "<main>",
                            "lineno": 14,
                            "in_app": true,
                            "filename": "app.rb",
                            "pre_context": [
                                "endn",
                                "n",
                                "n"
                            ],
                            "context_line": "Sentry.capture_message("Testing capture_message fropm Ruby Sentry SDK")n",
                            "post_context": [
                                null,
                                null,
                                null
                            ]
                        },
                        {
                            "project_root": "/Users/nickilieskou/projects/error-tracking-test/messages/ruby",
                            "abs_path": "/Users/nickilieskou/.asdf/installs/ruby/3.0.4/lib/ruby/gems/3.0.0/gems/sentry-ruby-5.7.0/lib/sentry-ruby.rb",
                            "function": "capture_message",
                            "lineno": 412,
                            "in_app": false,
                            "filename": "sentry-ruby.rb",
                            "pre_context": [
                                "    # @return [Event, nil]n",
                                "    def capture_message(message, **options, &block)n",
                                "      return unless initialized?n"
                            ],
                            "context_line": "      get_current_hub.capture_message(message, **options, &block)n",
                            "post_context": [
                                "    endn",
                                "n",
                                "    # Takes an instance of Sentry::Event and dispatches it to the currently active hub.n"
                            ]
                        },
                        {
                            "project_root": "/Users/nickilieskou/projects/error-tracking-test/messages/ruby",
                            "abs_path": "/Users/nickilieskou/.asdf/installs/ruby/3.0.4/lib/ruby/gems/3.0.0/gems/sentry-ruby-5.7.0/lib/sentry/hub.rb",
                            "function": "capture_message",
                            "lineno": 145,
                            "in_app": false,
                            "filename": "sentry/hub.rb",
                            "pre_context": [
                                "      options[:hint] ||= {}n",
                                "      options[:hint][:message] = messagen",
                                "      backtrace = options.delete(:backtrace)n"
                            ],
                            "context_line": "      event = current_client.event_from_message(message, options[:hint], backtrace: backtrace)n",
                            "post_context": [
                                "n",
                                "      return unless eventn",
                                "n"
                            ]
                        }
                    ]
                }
            }
        ]
    }
}
```