# error-tracking-test Golang - Message Interface

In order to run this application make sure that `SENTRY_DSN` env var exists

```
export SENTRY_DSN=<SENTRY_DSN>
go run main.go
```

The payload that is emitted with `captureMessage` is being sent to the Store endpoint and looks like the following:
```
{
    "contexts": {
        "device": {
            "arch": "amd64",
            "num_cpu": 10
        },
        "os": {
            "name": "darwin"
        },
        "runtime": {
            "go_maxprocs": 10,
            "go_numcgocalls": 1,
            "go_numroutines": 2,
            "name": "go",
            "version": "go1.19.2"
        }
    },
    "environment": "dev",
    "event_id": "42161e8f888048e0941452a1eaab3dca",
    "level": "info",
    "message": "Testing CaptureMessage from Go",
    "platform": "go",
    "release": "v1.0.0",
    "sdk": {
        "name": "sentry.go",
        "version": "0.13.0",
        "integrations": [
            "ContextifyFrames",
            "Environment",
            "IgnoreErrors",
            "Modules"
        ],
        "packages": [
            {
                "name": "sentry-go",
                "version": "0.13.0"
            }
        ]
    },
    "server_name": "Nicks-MacBook-Pro.local",
    "user": {},
    "modules": {
        "": "",
        "github.com/getsentry/sentry-go": "v0.13.0",
        "golang.org/x/sys": "v0.0.0-20211007075335-d3039528d8ac"
    },
    "timestamp": "2023-01-12T10:16:09.920562+01:00"
}
```

With `AttachStacktrace` to true the stacktrace is added to the payload:
```
{
    "contexts": {
        "device": {
            "arch": "amd64",
            "num_cpu": 10
        },
        "os": {
            "name": "darwin"
        },
        "runtime": {
            "go_maxprocs": 10,
            "go_numcgocalls": 1,
            "go_numroutines": 2,
            "name": "go",
            "version": "go1.19.2"
        }
    },
    "environment": "dev",
    "event_id": "3191040ccdce40799ef1e337e2602341",
    "level": "info",
    "message": "Testing CaptureMessage from Go",
    "platform": "go",
    "release": "v1.0.0",
    "sdk": {
        "name": "sentry.go",
        "version": "0.13.0",
        "integrations": [
            "ContextifyFrames",
            "Environment",
            "IgnoreErrors",
            "Modules"
        ],
        "packages": [
            {
                "name": "sentry-go",
                "version": "0.13.0"
            }
        ]
    },
    "server_name": "Nicks-MacBook-Pro.local",
    "threads": [
        {
            "stacktrace": {
                "frames": [
                    {
                        "function": "main",
                        "module": "main",
                        "abs_path": "/Users/nickilieskou/projects/error-tracking-test/messages/go/main.go",
                        "lineno": 85,
                        "pre_context": [
                            "t}",
                            "t// Flush buffered events before the program terminates.",
                            "t// Set the timeout to the maximum duration the program can afford to wait.",
                            "tdefer sentry.Flush(2 * time.Second)",
                            ""
                        ],
                        "context_line": "tsentry.CaptureMessage("Testing CaptureMessage from Go")",
                        "post_context": [
                            "}",
                            ""
                        ],
                        "in_app": true
                    }
                ]
            },
            "current": true
        }
    ],
    "user": {},
    "modules": {
        "": "",
        "github.com/getsentry/sentry-go": "v0.13.0",
        "golang.org/x/sys": "v0.0.0-20211007075335-d3039528d8ac"
    },
    "timestamp": "2023-01-12T11:05:17.217234+01:00"
}
```