require "sentry-ruby"



Sentry.init do |config|
  config.dsn =  ENV["SENTRY_DSN"]
  config.debug = true
  config.breadcrumbs_logger = [:sentry_logger, :http_logger]
end

class RubySdkTestError < StandardError
    def message
      "Hello from Ruby Sentry SDK"
    end
end


begin
  raise RubySdkTestError
  rescue RubySdkTestError => exception
    Sentry.capture_exception(exception)
end
