import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import * as Sentry from "@sentry/angular";
import { AppModule } from './app/app.module';

Sentry.init({
  dsn: "http://user@localhost:8080/projects/32149347",
  environment: `example`,
  enabled: true,
  release: "v1.1.1"
});


platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));

function myUndefinedFunction(){
  throw new Error('Testing Angular Sentry SDK');
}
myUndefinedFunction();
