# error-tracking-test Angular


Install Angular:
```
npm install -g @angular/cli
```

edit main.ts and add your sentry dsn and then execute:

```
cd exceptions/angular/my-app
ng serve --open
```

Angular SDK sends error events using the POST envelope

